export const portals: any[] = [
  {
    name: 'connect',
    text: 'Use a trusted partner to share, link, disseminate and monitor your research.'
  },
  {
    name: 'monitor',
    text: 'Use our monitoring services and easily track all relevant research outcomes.'
  },
  {
    name: 'explore',
    text: 'Explore all OA research outcomes. Link your research. Build your profile.'
  },
  {
    name: 'provide',
    text: 'Join OpenAIRE, user our tools and make your content more visible around the world.'
  },
  {
    name: 'osmonitor',
    text: 'An OpenAIRE service to better understand the European open research landscape, track trends for open access to publications, data, software, reveal hidden potential on existing resources and view open collaboration patterns.'
  },
]
