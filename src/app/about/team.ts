export interface member {
  name: string,
  role: string,
  affiliation: string,
  country: string,
  responsibilities: string,
  photo: string,
  active?: boolean
}

export const team: member[] = [
  {
    name: 'Alessia Bardi',
    role: 'Researcher / Product Manager of the OpenAIRE Research Community Dashboard (CONNECT)',
    affiliation: 'Institute of Information Science and Technologies, Italian National Research Council',
    country: 'Italy',
    responsibilities: 'Design and operation of the pipeline for the materialisation and data quality evaluation of the graph.',
    photo: 'alessia.jpg'
  },
  {
    name: 'Andrea Dell Amico',
    role: 'Τechnical Team - Systems Administrator',
    affiliation: 'Institute of Information Science and Technologies, Italian National Research Council',
    country: 'Italy',
    responsibilities: 'Computing and storage infrastructure (CNR), maintaining the Hadoop and ElasticSearch clusters.',
    photo: 'dell_amico.jpg'
  },
  {
    name: 'Andrea Mannocci',
    role: 'Researcher/Data scientist',
    affiliation: 'Institute of Information Science and Technologies, Italian National Research Council',
    country: 'Italy',
    responsibilities: 'Data analysis and quality of data.',
    photo: 'mannocci.jpg'
  },
  {
    name: 'Andreas Czerniak',
    role: 'Software and Data Engineer / Data Quality',
    affiliation: 'Bielefeld University Library',
    country: 'Germany',
    responsibilities: 'Aggregation (collection and transformation) of metadata.',
    photo: 'czerniak.jpg'
  },
  // {
  //   name: 'Antonis Lempesis',
  //   role: '',
  //   affiliation: '',
  //   country: 'Athens',
  //   responsibilities: '',
  //   photo: ''
  // },
  {
    name: 'Claudio Atzori',
    role: 'Software & Data Engineer / Data quality',
    affiliation: 'Institute of Information Science and Technologies, Italian National Research Council',
    country: 'Italy',
    responsibilities: 'Design of graph processing pipeline, gluing all stages, from the content aggregation, to indexing.',
    photo: 'claudio.jpg'
  },
  {
    name: 'Eleni Zacharia-Lamprou',
    role: 'Software Engineer - Postdoctoral Researcher',
    affiliation: 'Athena Research Center (ARC)',
    country: 'Greece',
    responsibilities: 'Implementation of Text and Data Mining (TDM) modules, used in the graph Enrichment phase.',
    photo: 'eleni.jpg'
  },
  // {
  //   name: 'Enrico Ottonello',
  //   role: 'Technical Team',
  //   affiliation: 'Institute of Information Science and Technologies, Italian National Research Council',
  //   country: 'Italy',
  //   responsibilities: 'Data engineering, aggregation, data curation along the graph enrichment steps.',
  //   photo: 'enrico.jpg'
  // },
  // {
  //   name: 'Giambattista Bloisi',
  //   role: '',
  //   affiliation: '',
  //   country: '',
  //   responsibilities: '',
  //   photo: ''
  // },
  {
    name: 'Gina Pavone',
    role: 'Open Science expert / Data Quality',
    affiliation: 'Institute of Information Science and Technologies, Italian National Research Council',
    country: 'Italy',
    responsibilities: 'Validating graph data quality, deduplication of organizations.',
    photo: 'gina.jpg'
  },
  {
    name: 'Harry Dimitropoulos',
    role: 'Senior Scientific Associate',
    affiliation: 'Athena Research Center (ARC)',
    country: 'Greece',
    responsibilities: 'Lead the Text and Data Mining (TDM) modules used in the graph Enrichment phase.',
    photo: 'dimitropoulos.jpg'
  },
  {
    name: 'Ioanna Grypari',
    role: 'Data Analyst / OpenAIRE Observatory, OpenAIRE MONITOR product manager',
    affiliation: 'Athena Research Center (ARC)',
    country: 'Greece',
    responsibilities: 'Analysis of the data to identify Open Science indicators for funders, research communities, institutions, and scientists.',
    photo: 'ioanna.jpg'
  },
  {
    name: 'Lampros Smyrnaios',
    role: 'Software & Data Engineer / Research Assistant',
    affiliation: 'Athena Research Center (ARC)',
    country: 'Greece',
    responsibilities: 'Implementation of Text and Data Mining (TDM) modules, used in the graph Enrichment phase.',
    photo: 'lampros.jpg'
  },
  // {
  //   name: 'Leonidas Pispiringas',
  //   role: '',
  //   affiliation: '',
  //   country: 'Greece',
  //   responsibilities: '',
  //   photo: ''
  // },
  {
    name: 'Marek Horst',
    role: 'Virtual Access Manager and Infrastructure Manager',
    affiliation: 'ICM, University of Warsaw',
    country: 'Poland',
    responsibilities: 'Quality of services, OpenAIRE Mining Infrastructure manager.',
    photo: 'marek.jpg'
  },
  {
    name: 'Michal Politowski',
    role: 'Technical Team - Systems Administrator',
    affiliation: 'ICM, University of Warsaw',
    country: 'Poland',
    responsibilities: 'Quality of services, software updates,  service operation management, deployment and monitoring of services.',
    photo: 'michal.png'
  },
  {
    name: 'Michele Artini',
    role: 'Software & Data Engineer / Data Quality',
    affiliation: 'Institute of Information Science and Technologies, Italian National Research Council',
    country: 'Italy',
    responsibilities: 'OpenOrgs design and development, software developer for Graph generation workflows and related micro-services.',
    photo: 'michele.png'
  },
  {
    name: 'Michele De Bonis',
    role: 'Technical Team',
    affiliation: 'Institute of Information Science and Technologies, Italian National Research Council',
    country: 'Italy',
    responsibilities: 'Deduplication phase and the creation of algorithms to identify groups of data into the graph.',
    photo: 'de_bonis.jpg'
  },
  {
    name: 'Miriam Baglioni',
    role: 'Researcher / Data Quality',
    affiliation: 'Institute of Information Science and Technologies, Italian National Research Council',
    country: 'Italy',
    responsibilities: 'Design and operation of the pipeline for the materialization and data quality evaluation of the graph.',
    photo: 'miriam.png'
  },
  {
    name: 'Paolo Manghi',
    role: 'Chief Technical Officer',
    affiliation: 'OpenAIRE AMKE / CNR, Institute of Information Science and Technologies',
    country: 'Italy',
    responsibilities: 'Design, roadmapping of the OpenAIRE infrastructure services, their operation, evolution, and interaction with third-parties.',
    photo: 'paolo.png'
  },
  // {
  //   name: 'Sabeel',
  //   role: '',
  //   affiliation: '',
  //   country: '',
  //   responsibilities: '',
  //   photo: ''
  // },
  {
    name: 'Sandro La Bruzzo',
    role: 'Technical Team',
    affiliation: 'Institute of Information Science and Technologies, Italian National Research Council',
    country: 'Italy',
    responsibilities: 'Graph enrichment steps including the generation of DOIBoost.',
    photo: 'sandro.jpg'
  },
  {
    name: 'Serafeim Chatzopoulos',
    role: 'Researcher /Software & Data Engineer',
    affiliation: 'OpenAIRE AMKE',
    country: 'Greece',
    responsibilities: 'Software developer for Graph generation workflows and related micro-services.',
    photo: 'serafeim.jpg'
  },
  {
    name: 'Thanasis Vergoulis',
    role: 'Development & Operator Director',
    affiliation: 'OpenAIRE AMKE',
    country: 'Greece',
    responsibilities: 'Technical roadmap management and supervision, ensuring quality of services.',
    photo: 'thanasis.png'
  },
  {
    name: 'Yannis Foufoulas',
    role: 'Software Engineer / Researcher',
    affiliation: 'Athena Research Center (ARC)',
    country: 'Greece',
    responsibilities: 'Implementation of Text and Data Mining (TDM) modules used in the graph Enrichment phase.',
    photo: 'yiannis.jpg'
  }
]
