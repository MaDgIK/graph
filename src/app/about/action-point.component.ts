import {AfterViewInit, Component} from "@angular/core";
import {animate, state, style, transition, trigger} from "@angular/animations";

@Component({
  selector: 'action-point',
  template: `
    <div class="uk-position-relative" style="width: 15px; height: 15px;">
      <div>
        <svg xmlns="http://www.w3.org/2000/svg" width="15" height="15" viewBox="0 0 15 15" fill="none">
          <circle cx="7.5" cy="7.5" r="7.5" fill="var(--portal-main-color)"/>
        </svg>
      </div>
      <div class="uk-position-top-left" [@move]="state" (@move.done)="onEnd($event)">
        <svg width="15" height="15" viewBox="0 0 15 15" fill="none">
          <circle cx="7.5" cy="6.5" r="5" stroke="var(--portal-main-color)" stroke-width="2.5" fill="none" />
        </svg>
      </div>
    </div>
  `,
  animations: [
    trigger('move', [
      state('in', style({
        transform: 'scale(1)',
        opacity: 1
      })),
      state('out', style({
        transform: 'scale(3)',
        opacity: 0
      })),
      transition('in => out', animate('2s ease-out'))
    ]),
  ]
})
export class ActionPointComponent implements AfterViewInit {
  state = 'in';
  
  ngAfterViewInit() {
    setTimeout(() => {
      this.state = 'out';
    }, 0);
  }
  
  onEnd(event) {
    this.state = 'in';
    if (event.toState === 'in') {
      setTimeout(() => {
        this.state = 'out';
      }, 2000);
    }
  }
}
