import {Component, OnDestroy, OnInit} from '@angular/core';
import {Breadcrumb} from '../openaireLibrary/utils/breadcrumbs/breadcrumbs.component';
import {member, team} from './team';
import {PiwikService} from '../openaireLibrary/utils/piwik/piwik.service';
import {Meta, Title} from '@angular/platform-browser';
import {SEOService} from '../openaireLibrary/sharedComponents/SEO/SEO.service';
import {EnvProperties} from '../openaireLibrary/utils/properties/env-properties';
import {properties} from '../../environments/environment';
import {Subscription} from 'rxjs';
import {Router} from '@angular/router';
import {HelperFunctions} from '../openaireLibrary/utils/HelperFunctions.class';

@Component({
  selector: 'team',
  template: `
    <schema2jsonld [URL]="properties.domain + '/about/team'"
                   [logoURL]="properties.domain + '/assets/common-assets/logo-small-graph.png'"
                   [description]="description"  type="other" [name]="title">
    </schema2jsonld>
    <div>
      <div class="uk-section">
        <div class="uk-margin-large-left uk-margin-medium-bottom">
          <breadcrumbs [breadcrumbs]="breadcrumbs"></breadcrumbs>
        </div>
        <div class="uk-container">
          <h2 class="uk-text-center">Meet The Team</h2>
          <div class="uk-padding-small">
            <div class="uk-grid uk-child-width-1-3@m uk-child-width-1-2@s uk-child-width-1-1 uk-margin-large-top" uk-grid>
              <div *ngFor="let member of team;" class="uk-card uk-card-flip" [class.uk-active]="member.active === true">
                <div class="uk-card-flip-inner">
                  <div class="front clickable" (click)="member.active = true">
                    <div class="uk-padding">
                      <div class="uk-text-center">
                        <img [src]="'assets/graph-assets/about/team/' + member.photo">
                        <div class="uk-margin-medium-top role">
                          <h6 class="portal-color">{{member.name}}</h6>
                          <div class="uk-text-muted">
                            {{member.role}}
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="uk-position-bottom-center uk-margin-bottom">
                      <a class="portal-link">Learn more</a>
                    </div>
                  </div>
                  <div class="back clickable" (click)="member.active = false">
                    <div class="uk-padding">
                      <div class="uk-text-center">
                        <img [src]="'assets/graph-assets/about/team/' + member.photo">
                        <div class="uk-margin-medium-top">
                          <h6 class="uk-text-center portal-color">{{member.name}}</h6>
                        </div>
                      </div>
                       <div class="uk-margin-small-bottom">
                         <span class="uk-text-bold">Responsibilities: </span> {{member.responsibilities}}
                       </div>
                      <div class="uk-margin-small-bottom">
                        <span class="uk-text-bold">Affiliation: </span> {{member.affiliation}}, {{member.country}}
                      </div>
                    </div>
                    <div class="uk-position-bottom-center uk-margin-bottom">
                      <a class="portal-link">Back</a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  `,
  styleUrls: ['team.component.css']
})
export class TeamComponent implements OnInit, OnDestroy  {
  public team: member[] = HelperFunctions.copy(team);
  properties: EnvProperties = properties;
  description = "The OpenAIRE Graph Team";
  title = "OpenAIRE - Graph | Team";
  subs: Subscription[] = [];
  public breadcrumbs: Breadcrumb[] = [
    {
      name: 'home',
      route: '/'
    },
    {
      name: 'about',
      route: '/about'
    },
    {
      name: 'Team'
    }
  ];
  
  constructor(private _title: Title, private _piwikService:PiwikService,
              private _router: Router, private _meta: Meta,
              private seoService: SEOService) {
  }
  
  ngOnInit() {
    this._title.setTitle(this.title);
    this._meta.updateTag({content:this.description},"name='description'");
    this._meta.updateTag({content:this.description},"property='og:description'");
    this._meta.updateTag({content:this.title},"property='og:title'");
    var url = this.properties.domain + this.properties.baseLink+this._router.url;
    this.seoService.createLinkForCanonicalURL(url, false);
    this._meta.updateTag({content:url},"property='og:url'");
    if(this.properties.enablePiwikTrack && (typeof document !== 'undefined')){
      this.subs.push(this._piwikService.trackView(this.properties, this.title).subscribe());
    }
  }
  
  public ngOnDestroy() {
    this.subs.forEach(sub => {
      if(sub instanceof Subscription) {
        sub.unsubscribe();
      }
    });
  }
}
