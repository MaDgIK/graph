import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {RouterModule} from "@angular/router";

import {AboutComponent} from "./about.component";
import {BreadcrumbsModule} from "../openaireLibrary/utils/breadcrumbs/breadcrumbs.module";
import {ActionPointComponent} from "./action-point.component";
import {IconsModule} from "../openaireLibrary/utils/icons/icons.module";
import {IconsService} from "../openaireLibrary/utils/icons/icons.service";
import {arrow_right} from "../openaireLibrary/utils/icons/icons";
import {Schema2jsonldModule} from '../openaireLibrary/sharedComponents/schema2jsonld/schema2jsonld.module';
import {NumbersModule} from '../openaireLibrary/sharedComponents/numbers/numbers.module';
import {TeamComponent} from './team.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild([
      {path: '', component: AboutComponent},
      {path: 'team', component: TeamComponent},
    ]),
    BreadcrumbsModule,
    IconsModule,
    Schema2jsonldModule,
    NumbersModule
  ],
  declarations: [AboutComponent, ActionPointComponent, TeamComponent],
  exports: [AboutComponent]
})
export class AboutModule {
  
  constructor(private iconsService: IconsService) {
    this.iconsService.registerIcons([arrow_right])
  }
}
