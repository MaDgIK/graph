import {Component, ElementRef, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Meta, Title} from '@angular/platform-browser';
import {Breadcrumb} from "../openaireLibrary/utils/breadcrumbs/breadcrumbs.component";
import {EnvProperties} from '../openaireLibrary/utils/properties/env-properties';
import {properties} from '../../environments/environment';
import {Subscription} from 'rxjs';
import {PiwikService} from '../openaireLibrary/utils/piwik/piwik.service';
import {SEOService} from '../openaireLibrary/sharedComponents/SEO/SEO.service';

declare var UIkit: any;

@Component({
  selector: 'about',
  templateUrl: 'about.component.html',
  styleUrls: ['about.component.css'],
})
export class AboutComponent implements OnInit, OnDestroy {
  @ViewChild('tabs') tabs: ElementRef;
  public architectureImage: string = "gray.png";
  public aggregationReadMore: boolean = false;
  public dedupClusteringReadMore: boolean = false;
  public dedupMatchingAndElectionReadMore: boolean = false;
  public enrichmentPropagationReadMore: boolean = false;
  public enrichmentMiningReadMore: boolean = false;
  properties:EnvProperties = properties;
  subs: Subscription[] = [];
  description = "The OpenAIRE Graph includes metadata and links between scientific products (e.g. literature, datasets, software, other research products), organizations, funders, funding streams, projects, communities, and (provenance) data sources.  Information on how we build it, the architecture, the infrastructure and the team.";
  title = "OpenAIRE - Graph | About";
  public breadcrumbs: Breadcrumb[] = [
    {
      name: 'home',
      route: '/'
    },
    {
      name: 'about'
    }
  ];

  constructor(private _title: Title,
              private route: ActivatedRoute, private _router: Router,
              private _piwikService:PiwikService,
              private _meta: Meta,  private seoService: SEOService) {
  }
  
  ngOnInit() {
    this._title.setTitle(this.title);
    this._meta.updateTag({content:this.description},"name='description'");
    this._meta.updateTag({content:this.description},"property='og:description'");
    this._meta.updateTag({content:this.title},"property='og:title'");
    var url = this.properties.domain + this.properties.baseLink+this._router.url;
    this.seoService.createLinkForCanonicalURL(url, false);
    this._meta.updateTag({content:url},"property='og:url'");
    if(this.properties.enablePiwikTrack && (typeof document !== 'undefined')){
      this.subs.push(this._piwikService.trackView(this.properties, this.title).subscribe());
    }
  }

  changeTab(index: number) {
    UIkit.switcher(this.tabs.nativeElement).show(index);
  }
  
  public ngOnDestroy() {
    this.subs.forEach(sub => {
      if(sub instanceof Subscription) {
        sub.unsubscribe();
      }
    });
  }
}
