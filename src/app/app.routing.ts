import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ErrorPageComponent} from './openaireLibrary/error/errorPage.component';
import {PreviousRouteRecorder} from './openaireLibrary/utils/piwik/previousRouteRecorder.guard';

const appRoutes: Routes = [
  {
    path: '',
    loadChildren: () => import('./home/home.module').then(m => m.HomeModule),
    data: {
      isHome: true
    }, canDeactivate: [PreviousRouteRecorder]
  },
  // {
  //   path: 'resources',
  //   loadChildren: () => import('./resources/resources.module').then(m => m.ResourcesModule),
  //   canDeactivate: [PreviousRouteRecorder]
  // },
  {
    path: 'support',
    loadChildren: () => import('./contact/contact.module').then(m => m.ContactModule),
    canDeactivate: [PreviousRouteRecorder]
  },
  {
    path: 'about',
    loadChildren: () => import('./about/about.module').then(m => m.AboutModule),
    canDeactivate: [PreviousRouteRecorder]
  },
  { path: '**',pathMatch: 'full',component: ErrorPageComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(appRoutes, {
    onSameUrlNavigation: "reload"
  })],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
