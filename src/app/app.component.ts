import {Component, OnDestroy, OnInit} from '@angular/core';
import {MenuItem, RootMenuItem} from './openaireLibrary/sharedComponents/menu';
import {EnvProperties} from './openaireLibrary/utils/properties/env-properties';
import {User} from './openaireLibrary/login/utils/helper.class';
import {properties} from '../environments/environment';
import {LayoutService} from './services/layout.service';
import {Header} from './openaireLibrary/sharedComponents/navigationBar.component';
import {SmoothScroll} from './openaireLibrary/utils/smooth-scroll';

@Component({
  selector: 'app',
  templateUrl: './app.component.html',
})

export class AppComponent implements OnInit, OnDestroy {
  title = 'OpenAIRE - Graph';
  userMenuItems: MenuItem[] = [];
  menuItems: RootMenuItem [] = [];
  logInUrl = null;
  logOutUrl = null;
  properties: EnvProperties = properties;
  showMenu: boolean = false;
  user: User;
  header: Header;
  logoPath: string = 'assets/common-assets/';
  
  constructor(private layoutService: LayoutService,
              private smoothScroll: SmoothScroll) {}
  
  ngOnInit() {
    this.logInUrl = this.properties.loginUrl;
    this.logOutUrl = this.properties.logoutUrl;
    this.showMenu = true;
    this.header = {
      route: "/",
      url: null,
      title: 'graph',
      logoUrl: this.logoPath + 'logo-large-graph.png',
      logoSmallUrl:this.logoPath + 'logo-small-graph.png',
      position:'left',
      badge:true
    };
    this.layoutService.isHome.subscribe(isHome => {
      this.buildMenu(isHome);
    });
  }
  
  ngOnDestroy() {
    this.smoothScroll.clearSubscriptions();
  }
  
  buildMenu(isHome) {
    this.menuItems = [
      {
        rootItem: new MenuItem("about", "About", "", "/about", false, [], null, {}),
        items: [
          new MenuItem("overview", "Overview", "", "/about", false, [], null, {}),
          new MenuItem("metrics", "Data and Metrics", "", "/about", false, [], null, {}, null, null, 'metrics'),
          new MenuItem("infrastructure", "Infrastructure", "", "/about", false, [], null, {}, null, null, 'infrastructure'),
          new MenuItem("team", "Team", "", "/about", false, [], null, {}, null, null, 'team')
        ]
      },
			{
				rootItem: new MenuItem("resources", "Resources", "", "", false, [], null, {}),
				items: [
					new MenuItem("documentation", "Documentation", "/docs", "", false, [], null, {}),
					new MenuItem("data", "Data", "/docs/category/downloads", "", false, [], null, {}),
					new MenuItem("apis", "APIs", "/develop/overview.html", "", false, [], null, {}, [
						new MenuItem("", "Overview", "/develop/overview.html", "", false, [], null, {}),
						new MenuItem("", "Authentication", "/develop/authentication.html", "", false, [], null, {}),
						new MenuItem("", "Selective Access", "/develop/api.html", "", false, [], null, {}),
						new MenuItem("", "Response Metadata Format", "/develop/response-metadata-format.html", "", false, [], null, {}),
					]),
					new MenuItem("publications", "Publications", "/docs/publications", "", false, [], null, {})
				]
			},
      {
        rootItem: new MenuItem("contact", "Support", "", "/support", false, [], null, {}),
        items: []
      }
    ];
    if(!isHome) {
      this.menuItems = [{
        rootItem: new MenuItem("home", "Home", "", "/", false, [], null, {}),
        items: []
      }].concat(this.menuItems);
    }
  }
}
