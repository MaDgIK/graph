import {Component, OnDestroy, OnInit} from '@angular/core';
import {Meta, Title} from '@angular/platform-browser';
import {Breadcrumb} from "../openaireLibrary/utils/breadcrumbs/breadcrumbs.component";
import {ActivatedRoute, Router} from '@angular/router';
import {properties} from '../../environments/environment';
import {EnvProperties} from '../openaireLibrary/utils/properties/env-properties';
import {PiwikService} from '../openaireLibrary/utils/piwik/piwik.service';
import {SEOService} from '../openaireLibrary/sharedComponents/SEO/SEO.service';
import {Subscription} from 'rxjs';

@Component({
  selector: 'resources',
  templateUrl: 'resources.component.html',
  styleUrls: ['resources.component.css'],
})
export class ResourcesComponent implements OnInit, OnDestroy {
  properties:EnvProperties;
  subs: Subscription[] = [];
  description = "Start building with OpenAIRE APIs. How to access the graph?  XML Metadata  schema and documentation.";
  title = "OpenAIRE - Graph | Resources";
  public breadcrumbs: Breadcrumb[] = [
    {
      name: 'home',
      route: '/'
    },
    {
      name: 'resources'
    }
  ];
  
  constructor(private _title: Title, private route: ActivatedRoute, private _router: Router,
              private _piwikService:PiwikService,
              private _meta: Meta,  private seoService: SEOService) {
  }
  
  ngOnInit() {
    this.properties = properties;
    this._title.setTitle(this.title);
    this._meta.updateTag({content:this.description},"name='description'");
    this._meta.updateTag({content:this.description},"property='og:description'");
    this._meta.updateTag({content:this.title},"property='og:title'");
    var url = this.properties.domain + this.properties.baseLink+this._router.url;
    this.seoService.createLinkForCanonicalURL(url, false);
    this._meta.updateTag({content:url},"property='og:url'");
    if(this.properties.enablePiwikTrack && (typeof document !== 'undefined')){
      this.subs.push(this._piwikService.trackView(this.properties, this.title).subscribe());
    }
  }
  
  public ngOnDestroy() {
    this.subs.forEach(sub => {
      if(sub instanceof Subscription) {
        sub.unsubscribe();
      }
    });
  }
}
