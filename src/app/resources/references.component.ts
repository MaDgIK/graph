import {Component, OnDestroy, OnInit} from '@angular/core';
import {Breadcrumb} from '../openaireLibrary/utils/breadcrumbs/breadcrumbs.component';
import {EnvProperties} from '../openaireLibrary/utils/properties/env-properties';
import {Subscription} from 'rxjs';
import {properties} from '../../environments/environment';
import {Meta, Title} from '@angular/platform-browser';
import {PiwikService} from '../openaireLibrary/utils/piwik/piwik.service';
import {Router} from '@angular/router';
import {SEOService} from '../openaireLibrary/sharedComponents/SEO/SEO.service';

@Component({
  selector: 'references',
  template: `
    <schema2jsonld [URL]="properties.domain + '/resources/references'"
                   [logoURL]="properties.domain + '/assets/common-assets/logo-small-graph.png'"
                   [description]="description" type="other" [name]="title">
    </schema2jsonld>
    <div>
      <div class="uk-section">
        <div class="uk-margin-large-left uk-margin-medium-bottom">
          <breadcrumbs [breadcrumbs]="breadcrumbs"></breadcrumbs>
        </div>
        <div class="uk-container">
          <h2 class="uk-text-center">How to cite the graph</h2>
          <div class="uk-padding-small">
            <ul class="portal-circle">
              <li>If you use one of the <a href="https://zenodo.org/record/4201546" target="_blank">dumps of the OpenAIRE Graph</a>,
                please cite it following the recommendation that you find on the Zenodo page.
              </li>
              <li><span class="uk-text-bold">All datasets and software</span> are available in our Zenodo community for the OpenAIRE
                Graph: <a href="https://zenodo.org/communities/openaire-research-graph" target="_blank">https://zenodo.org/communities/openaire-research-graph</a>
              </li>
            </ul>
          </div>
        </div>
      </div>
      <div class="uk-container uk-section">
        <h2 class="uk-text-center">References on the graph pipeline</h2>
        <div class="uk-padding-small">
          <div class="uk-padding-large uk-padding-remove-horizontal uk-padding-remove-bottom uk-margin-large-bottom">
            <h3 class="portal-color">Aggregation system</h3>
            <ul class="portal-circle uk-margin-medium-top">
              <li>
                <a href="https://explore.openaire.eu/search/result?pid=10.1108/PROG-08-2013-0045" target="_blank">
                  Manghi, P., Artini, M., Atzori, C., Bardi, A., Mannocci, A., La Bruzzo, S., Candela, L., Castelli, D. and Pagano, P.
                  (2014), “The D-NET software toolkit: A framework for the realization, maintenance, and operation of aggregative
                  infrastructures”, Program: electronic library and information systems, Vol. 48 No. 4, pp. 322-354.
                  <span class="custom-external custom-icon space"></span>
                </a>
              </li>
              <li>
                <a href="https://explore.openaire.eu/search/result?pid=10.5281/zenodo.168362" target="_blank">
                  Michele Artini, Claudio Atzori, Alessia Bardi, Sandro La Bruzzo, Paolo Manghi, & Andrea Mannocci. (2016, November 24). The
                  D-NET software toolkit: dnet-basic-aggregator (Version 1.3.0). Zenodo.
                  <span class="custom-external custom-icon space"></span>
                </a>
              </li>
              <li>
                Atzori, C., Bardi, A., Manghi, P., & Mannocci, A. (2017, January). The OpenAIRE workflows for data management. In Italian
                Research Conference on Digital Libraries (pp. 95-107). Springer, Cham.
              </li>
              <li>
                Mannocci, A., & Manghi, P. (2016, September). DataQ: a data flow quality monitoring system for aggregative data
                infrastructures. In International Conference on Theory and Practice of Digital Libraries (pp. 357-369). Springer, Cham.
              </li>
            </ul>
          </div>
          <div class="uk-margin-large-bottom">
            <h3 class="portal-color">Deduplication</h3>
            <ul class="portal-circle uk-margin-medium-top">
              <li>
                <a href="https://explore.openaire.eu/search/result?pid=10.5281/zenodo.4302081" target="_blank">
                  Claudio Atzori, & Paolo Manghi. (2017, February 17). gdup: a big graph entity deduplication system (Version 4.0.5).
                  Zenodo.
                  <span class="custom-external custom-icon space"></span>
                </a>
                <a href="https://code-repo.d4science.org/D-Net/dnet-dedup/releases" target="_blank">
                  https://code-repo.d4science.org/D-Net/dnet-dedup/releases
                  <span class="custom-external custom-icon space"></span>
                </a>
              </li>
              <li>
                <a href="https://explore.openaire.eu/search/result?pid=10.1504/IJMSO.2012.050014" target="_blank">
                  Manghi, Paolo, Marko Mikulicic, and Claudio Atzori. "De-duplication of aggregation authority files." International Journal
                  of Metadata, Semantics and Ontologies 7.2 (2012): 114-130.
                  <span class="custom-external custom-icon space"></span>
                </a>
              </li>
              <li>
                <a href="https://explore.openaire.eu/search/result?pid=10.1108/DTA-09-2019-0163" target="_blank">
                  Manghi, P., Atzori, C., De Bonis, M., & Bardi, A. (2020). Entity deduplication in big data graphs for scholarly
                  communication. Data Technologies and Applications.
                  <span class="custom-external custom-icon space"></span>
                </a>
              </li>
              <li>
                <a href="https://explore.openaire.eu/search/result?pid=10.1007/978-3-642-24731-6_8" target="_blank">
                  Manghi, P., & Mikulicic, M. (2011, October). PACE: A general-purpose tool for authority control. In Research Conference on
                  Metadata and Semantic Research (pp. 80-92). Springer, Berlin, Heidelberg.
                  <span class="custom-external custom-icon space"></span>
                </a>
              </li>
              <li>
                <a href="https://explore.openaire.eu/search/result?pid=10.1109/BDCAT.2018.00025" target="_blank">
                  Atzori, C., Manghi, P., & Bardi, A. (2018, December). GDup: de-duplication of scholarly communication big graphs. In 2018
                  IEEE/ACM 5th International Conference on Big Data Computing Applications and Technologies (BDCAT) (pp. 142-151). IEEE.
                  <span class="custom-external custom-icon space"></span>
                </a>
              </li>
              <li>
                <a href="https://explore.openaire.eu/search/result?pid=10.5281/zenodo.1454880" target="_blank">
                  Atzori, Claudio. "GDup: an Integrated, Scalable Big Graph Deduplication System." (2016).
                  <span class="custom-external custom-icon space"></span>
                </a>
              </li>
            </ul>
          </div>
          <div class="uk-margin-large-bottom">
            <h3 class="portal-color">Mining</h3>
            <ul class="portal-circle uk-margin-medium-top">
              <li>
                <a href="https://explore.openaire.eu/search/result?pid=10.1016/j.procs.2014.10.016" target="_blank">
                  M. Kobos, Ł. Bolikowski, M. Horst, P. Manghi, N. Manola, J. Schirrwagen, “Information inference in scholarly communication
                  infrastructures: the OpenAIREplus project experience”, Procedia Computer Science 38, 92-99.
                  <span class="custom-external custom-icon space"></span>
                </a>
              </li>
              <li>
                <a href="https://explore.openaire.eu/search/result?pid=10.1007/s10032-015-0249-8" target="_blank">
                  Tkaczyk, D., Szostek, P., Fedoryszak, M. et al. CERMINE: automatic extraction of structured metadata from scientific
                  literature. IJDAR 18, 317–335 (2015).
                  <span class="custom-external custom-icon space"></span>
                </a>
              </li>
              <li>
                <a href="https://explore.openaire.eu/search/result?pid=10.1007/978-3-030-11226-4_27" target="_blank">
                  Giannakopoulos T., Foufoulas Y., Dimitropoulos H., Manola N. (2019) “Interactive Text Analysis and Information
                  Extraction”. In: Manghi P., Candela L., Silvello G. (eds) Digital Libraries: Supporting Open Science. IRCDL 2019.
                  Communications in Computer and Information Science, vol 988. Springer, Cham.
                  <span class="custom-external custom-icon space"></span>
                </a>
              </li>
              <li>
                <a href="https://explore.openaire.eu/search/result?pid=10.1007/978-3-319-67008-9_28" target="_blank">
                  Foufoulas Y., Stamatogiannakis L., Dimitropoulos H., Ioannidis Y. (2017) “High-Pass Text Filtering for Citation Matching”.
                  In: Kamps J., Tsakonas G., Manolopoulos Y., Iliadis L., Karydis I. (eds) Research and Advanced Technology for Digital
                  Libraries. TPDL 2017. Lecture Notes in Computer Science, vol 10450. Springer, Cham.
                  <span class="custom-external custom-icon space"></span>
                </a>
              </li>
              <li>
                <a href="https://explore.openaire.eu/search/result?pid=10.1145/2740908.2742024" target="_blank">
                  T. Giannakopoulos, I. Foufoulas, E. Stamatogiannakis, H. Dimitropoulos, N. Manola, and Y. Ioannidis. 2015. “Visual-Based
                  Classification of Figures from Scientific Literature”. In Proceedings of the 24th International Conference on World Wide
                  Web
                  (WWW '15 Companion). Association for Computing Machinery, New York, NY, USA, 1059–1060.
                  <span class="custom-external custom-icon space"></span>
                </a>
              </li>
              <li>
                <a href="https://explore.openaire.eu/search/result?pid=10.1045/november14-giannakopoulos" target="_blank">
                  Giannakopoulos, T., Foufoulas, I., Stamatogiannakis, E., Dimitropoulos, H., Manola, N., & Ioannidis, Y. (2014).
                  “Discovering and Visualizing Interdisciplinary Content Classes in Scientific Publications”. D-Lib Mag., Volume 20, Number
                  11/12.
                  <span class="custom-external custom-icon space"></span>
                </a>
              </li>
              <li>
                <a href="https://explore.openaire.eu/search/result?pid=10.1007/978-3-319-08425-1_10" target="_blank">
                  Giannakopoulos T., Stamatogiannakis E., Foufoulas I., Dimitropoulos H., Manola N., Ioannidis Y. (2014) “Content
                  Visualization of Scientific Corpora Using an Extensible Relational Database Implementation”. In: Bolikowski Ł., Casarosa
                  V.,
                  Goodale P., Houssos N., Manghi P., Schirrwagen J. (eds) Theory and Practice of Digital Libraries -- TPDL 2013 Selected
                  Workshops. TPDL 2013. Communications in Computer and Information Science, vol 416. Springer, Cham.
                  <span class="custom-external custom-icon space"></span>
                </a>
                Also in: <a
                  href="https://books.google.gr/books?hl=en&lr=&id=EVMqBAAAQBAJ&oi=fnd&pg=PA101&dq=info:SJ9N406OYsEJ:scholar.google.com&ots=z-eB4d3wkv&sig=rEc-V3CBqcOD6gja6p5wLVHq4jc&redir_esc=y#v=onepage&q&f=false"
                  target="_blank">
                Google Books
                <span class="custom-external custom-icon space"></span>
              </a>
              </li>
              <li>
                <a href="https://explore.openaire.eu/search/result?pid=10.1007/978-3-642-38634-3_23" target="_blank">
                  Giannakopoulos T., Dimitropoulos H., Metaxas O., Manola N., Ioannidis Y. (2013) “Supervised Content Visualization of
                  Scientific Publications: A Case Study on the ArXiv Dataset”. In: Kłopotek M.A., Koronacki J., Marciniak M., Mykowiecka A.,
                  Wierzchoń S.T. (eds) Language Processing and Intelligent Information Systems. IIS 2013. Lecture Notes in Computer Science,
                  vol 7912. Springer, Berlin, Heidelberg.
                  <span class="custom-external custom-icon space"></span>
                </a>
              </li>
              <li>
                <a href="http://ceur-ws.org/Vol-1558/paper45.pdf"
                   target="_blank">
                  Y. Chronis, Y. Foufoulas, V. Nikolopoulos, A. Papadopoulos, L. Stamatogiannakis, C. Svingos, Y. E. Ioannidis, "A
                  Relational Approach to Complex Dataflows", in Workshop Proceedings of the EDBT/ICDT 2016 (MEDAL 2016) Joint Conference
                  (March 15, 2016, Bordeaux, France) on CEUR-WS.org (ISSN 1613-0073)
                  <span class="custom-external custom-icon space"></span>
                </a>
              </li>
            </ul>
          </div>
          <div class="uk-margin-large-bottom">
            <h3 class="portal-color">Portals</h3>
            <ul class="portal-circle uk-margin-medium-top">
              <li>
                <a href="https://explore.openaire.eu/search/result?pid=10.5281/zenodo.3467104" target="_blank">
                  Baglioni M. et al. (2019) The OpenAIRE Research Community Dashboard: On Blending Scientific Workflows and Scientific
                  Publishing. In: Doucet A., Isaac A., Golub K., Aalberg T., Jatowt A. (eds) Digital Libraries for Open Knowledge. TPDL 2019.
                  Lecture Notes in Computer Science, vol 11799. Springer, Cham.
                  <span class="custom-external custom-icon space"></span>
                </a>
              </li>
            </ul>
          </div>
          <div class="uk-margin-large-bottom">
            <h3 class="portal-color">Broker Service</h3>
            <ul class="portal-circle uk-margin-medium-top">
              <li>
                Artini, M., Atzori, C., Bardi, A., La Bruzzo, S., Manghi, P., & Mannocci, A. (2015). The OpenAIRE literature broker
                service for institutional repositories. D-Lib Magazine, 21(11/12), 1.
              </li>
              <li>Manghi, P., Atzori, C., Bardi, A., La Bruzzo, S., & Artini, M. (2016, February). Realizing a Scalable and History-Aware
                Literature Broker Service for OpenAIRE. In Italian Research Conference on Digital Libraries (pp. 92-103). Springer, Cham.
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  `,
  styleUrls: ['references.component.css']
})
export class ReferencesComponent implements OnInit, OnDestroy {
  properties: EnvProperties = properties;
  description = 'How to cite? Bibliographic references relative to the implementation of the OpenAIRE Graph processing pipeline and how to cite the graph results. ';
  title = 'OpenAIRE - Graph | References';
  subs: Subscription[] = [];
  public breadcrumbs: Breadcrumb[] = [
    {
      name: 'home',
      route: '/'
    },
    {
      name: 'Resources',
      route: '/resources'
    },
    {
      name: 'References'
    }
  ];
  
  constructor(private _title: Title, private _piwikService: PiwikService,
              private _router: Router, private _meta: Meta,
              private seoService: SEOService) {
  }
  
  ngOnInit() {
    this._title.setTitle(this.title);
    this._meta.updateTag({content: this.description}, 'name=\'description\'');
    this._meta.updateTag({content: this.description}, 'property=\'og:description\'');
    this._meta.updateTag({content: this.title}, 'property=\'og:title\'');
    var url = this.properties.domain + this.properties.baseLink + this._router.url;
    this.seoService.createLinkForCanonicalURL(url, false);
    this._meta.updateTag({content: url}, 'property=\'og:url\'');
    if (this.properties.enablePiwikTrack && (typeof document !== 'undefined')) {
      this.subs.push(this._piwikService.trackView(this.properties, this.title).subscribe());
    }
  }
  
  public ngOnDestroy() {
    this.subs.forEach(sub => {
      if (sub instanceof Subscription) {
        sub.unsubscribe();
      }
    });
  }
}
