import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {RouterModule} from "@angular/router";

import {ResourcesComponent} from "./resources.component";
import {BreadcrumbsModule} from "../openaireLibrary/utils/breadcrumbs/breadcrumbs.module";
import {Schema2jsonldModule} from '../openaireLibrary/sharedComponents/schema2jsonld/schema2jsonld.module';
import {ReferencesComponent} from './references.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild([
      {path: '', component: ResourcesComponent},
      {path: 'references', component: ReferencesComponent}
    ]),
    BreadcrumbsModule,
    Schema2jsonldModule
  ],
  declarations: [ResourcesComponent, ReferencesComponent],
  exports: [ResourcesComponent]
})
export class ResourcesModule {

}
