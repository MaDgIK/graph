import {AppRoutingModule} from "./app.routing";
import {BrowserModule} from "@angular/platform-browser";
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {CookieLawModule} from "./openaireLibrary/sharedComponents/cookie-law/cookie-law.module";
import {NavigationBarModule} from "./openaireLibrary/sharedComponents/navigationBar.module";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {SafeHtmlPipeModule} from "./openaireLibrary/utils/pipes/safeHTMLPipe.module";
import {AppComponent} from "./app.component";
import {NgModule} from "@angular/core";
import {BottomModule} from './openaireLibrary/sharedComponents/bottom.module';
import {Schema2jsonldModule} from './openaireLibrary/sharedComponents/schema2jsonld/schema2jsonld.module';
import {ErrorModule} from './openaireLibrary/error/error.module';
import {PreviousRouteRecorder} from './openaireLibrary/utils/piwik/previousRouteRecorder.guard';
import {PiwikService} from './openaireLibrary/utils/piwik/piwik.service';
import {DEFAULT_TIMEOUT, TimeoutInterceptor} from './openaireLibrary/timeout-interceptor.service';

@NgModule({
  imports: [
    AppRoutingModule,
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    NavigationBarModule,
    CookieLawModule,
    SafeHtmlPipeModule,
    BrowserAnimationsModule,
    BottomModule,
    Schema2jsonldModule,
    ErrorModule
  ],
    declarations: [
        AppComponent,
    ],
    providers: [PreviousRouteRecorder, PiwikService,
      [{ provide: HTTP_INTERCEPTORS, useClass: TimeoutInterceptor, multi: true }],
      [{ provide: DEFAULT_TIMEOUT, useValue: 30000 }]
    ],
    bootstrap: [ AppComponent ]
})
export class AppModule { }
