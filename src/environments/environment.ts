// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

import {EnvProperties} from "../app/openaireLibrary/utils/properties/env-properties";

export let properties: EnvProperties = {
  environment: "development",
  domain : "https://graph.openaire.eu",
  baseLink: "",
  enablePiwikTrack: false,
  piwikBaseUrl: "https://analytics.openaire.eu/piwik.php?idsite=",
  piwikSiteId: "",
  searchAPIURLLAst: "https://beta.services.openaire.eu/search/v2/api/",
  adminToolsAPIURL: 'http://duffy.di.uoa.gr:8080/uoa-admin-tools/',
  admins: ["kostis30fylloy@gmail.com"],
  cookieDomain: ".di.uoa.gr",
  reCaptchaSiteKey: "6LcVtFIUAAAAAB2ac6xYivHxYXKoUvYRPi-6_rLu",
  searchLinkToResult: "/search/result?id=",
  searchLinkToPublication: "/search/publication?articleId=",
  searchLinkToProject: "/search/project?projectId=",
  searchLinkToDataProvider: "/search/dataprovider?datasourceId=",
  searchLinkToDataset: "/search/dataset?datasetId=",
  searchLinkToSoftwareLanding: "/search/software?softwareId=",
  searchLinkToOrp: "/search/other?orpId=",
  searchLinkToOrganization: "/search/organization?organizationId=",
  searchLinkToPublications: "/search/find/publications",
  searchLinkToDataProviders: "/search/find/dataproviders",
  searchLinkToProjects: "/search/find/projects",
  searchLinkToDatasets: "/search/find/datasets",
  searchLinkToSoftware: "/search/find/software",
  searchLinkToOrps: "/search/find/other",
  searchLinkToOrganizations: "/search/find/organizations",
  searchLinkToCompatibleDataProviders: "/search/content-providers",
  searchLinkToEntityRegistriesDataProviders: "/search/entity-registries",
  searchLinkToEntityRegistriesDataProvidersTable: "/search/entity-registries-table",
  searchLinkToJournals: "/search/journals",
  searchLinkToJournalsTable: "/search/journals-table",
  searchLinkToResults: "/search/find/research-outcomes",
  searchLinkToAdvancedPublications: "/search/advanced/publications",
  searchLinkToAdvancedProjects: "/search/advanced/projects",
  searchLinkToAdvancedDatasets: "/search/advanced/datasets",
  searchLinkToAdvancedSoftware: "/search/advanced/software",
  searchLinkToAdvancedOrps: "/search/advanced/other",
  searchLinkToAdvancedDataProviders: "/search/advanced/dataproviders",
  searchLinkToAdvancedOrganizations: "/search/advanced/organizations",
  searchLinkToAdvancedResults: "/search/advanced/research-outcomes",
  footerGrantText: "OpenAIRE has received funding from the European Union's Horizon 2020 research and innovation programme under grant agreements No. 777541 and 101017452"
};
